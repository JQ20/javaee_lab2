package cn.edu.ujn.lab2.action;
import java.util.List;
import java.util.Map;
import cn.edu.ujn.lab2.dao.KcDao;
import cn.edu.ujn.lab2.dao.KcDaoImpl;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class KcAction extends ActionSupport{
	public String execute()throws Exception{
		KcDao kcDao=new KcDaoImpl();
		List list=kcDao.getAll();
		Map request=(Map)ActionContext.getContext().get("request");
		request.put("list", list);
		return SUCCESS;
	}

}
