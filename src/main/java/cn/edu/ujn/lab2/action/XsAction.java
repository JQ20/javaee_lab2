package cn.edu.ujn.lab2.action;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cn.edu.ujn.lab2.dao.KcDaoImpl;
import cn.edu.ujn.lab2.dao.XsDao;
import cn.edu.ujn.lab2.dao.XsDaoImpl;
import cn.edu.ujn.lab2.model.Dlb;
import cn.edu.ujn.lab2.model.Kcb;
import cn.edu.ujn.lab2.model.Xsb;



public class XsAction extends ActionSupport{
	
	private Kcb kcb;

	public Kcb getKcb() {
		return kcb;
	}



	public void setKcb(Kcb kcb) {
		this.kcb = kcb;
	}



	public String getXsKcs() throws Exception{
		Map session=(Map)ActionContext.getContext().getSession();
		Dlb user=(Dlb) session.get("user");
		String xh=user.getXh();
		//得到当前学生的信息
		Xsb xsb=new XsDaoImpl().getOneXs(xh);
		//取出选修的课程Set
		Set list=xsb.getKcs();
		Map request=(Map) ActionContext.getContext().get("request");
		//保存
		request.put("list",list);
		return SUCCESS;
	}


	
		public String deleteKc()throws Exception{
			Map session=(Map)ActionContext.getContext().getSession();
			String xh=((Dlb)session.get("user")).getXh();
			XsDao xsDao=new XsDaoImpl();
			Xsb xs2=xsDao.getOneXs(xh);
			Set list=xs2.getKcs();
			Iterator iter=list.iterator();
			while(iter.hasNext()){
				Kcb kc2=(Kcb)iter.next();
				if(kc2.getKch().equals(kcb.getKch())){
					iter.remove();
				}
			}
			xs2.setKcs(list);
			xsDao.update(xs2);
			return SUCCESS;
		}
		
		public String selectKc()throws Exception{
			Map session=(Map)ActionContext.getContext().getSession();
			String xh=((Dlb)session.get("user")).getXh();
			XsDao xsDao=new XsDaoImpl();
			Xsb xs3=xsDao.getOneXs(xh);
			Set list=xs3.getKcs();
			Iterator iter=list.iterator();
			while(iter.hasNext()){
			Kcb kc3=(Kcb)iter.next();
				if(kc3.getKch().equals(kcb.getKch())){
					return ERROR;
			}
			}
		list.add(new KcDaoImpl().getOneKc(kcb.getKch()));
			xs3.setKcs(list);
			xsDao.update(xs3);
			return SUCCESS;
		}


}
