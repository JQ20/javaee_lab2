package cn.edu.ujn.lab2.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.ujn.lab2.db.HibernateSessionFactory;
import cn.edu.ujn.lab2.model.Zyb;

public class ZyDaoImpl implements ZyDao {

	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.ZyDao#getOneZy(java.lang.Integer)
	 */
	@Override
	public Zyb getOneZy(Integer zyId) {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts=session.beginTransaction();
			Query query=session.createQuery("from Zyb where id=?1");
			query.setParameter(1, zyId);
			query.setMaxResults(1);
			Zyb zy=(Zyb) query.uniqueResult();
			ts.commit();
			HibernateSessionFactory.closeSession();
			return zy;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.ZyDao#getAll()
	 */
	@Override
	public List getAll() {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts=session.beginTransaction();
			List list=session.createQuery("from Zyb").list();
			ts.commit();
			HibernateSessionFactory.closeSession();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
