package cn.edu.ujn.lab2.dao;

import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.ujn.lab2.db.HibernateSessionFactory;
import cn.edu.ujn.lab2.model.Kcb;
import cn.edu.ujn.lab2.model.Xsb;

public class XsDaoImpl implements XsDao {
	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.XsDao#getOneXs(java.lang.String)
	 */
	@Override
	public Xsb getOneXs(String xh) {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts=session.beginTransaction();
			Query query=session.createQuery("from Xsb where xh=?1");
			query.setParameter(1, xh); 
			query.setMaxResults(1);
			Xsb xs=(Xsb) query.uniqueResult();
			Set<Kcb> kcs = xs.getKcs();
			System.out.println(kcs.size());
			ts.commit();
			session.clear();
			return xs;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.XsDao#update(cn.edu.ujn.lab2.model.Xsb)
	 */
	@Override
	public void update(Xsb xs) {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts=session.beginTransaction();
			session.update(xs);
			ts.commit();
			HibernateSessionFactory.closeSession();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
