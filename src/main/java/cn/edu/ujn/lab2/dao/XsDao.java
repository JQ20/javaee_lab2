package cn.edu.ujn.lab2.dao;

import cn.edu.ujn.lab2.model.Xsb;

public interface XsDao {

	Xsb getOneXs(String xh);

	void update(Xsb xs);

}