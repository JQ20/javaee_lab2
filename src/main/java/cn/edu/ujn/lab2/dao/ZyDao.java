package cn.edu.ujn.lab2.dao;

import java.util.List;

import cn.edu.ujn.lab2.model.Zyb;

public interface ZyDao {

	Zyb getOneZy(Integer zyId);

	List getAll();

}