package cn.edu.ujn.lab2.dao;

import java.util.List;

import cn.edu.ujn.lab2.model.Kcb;

public interface KcDao {

	Kcb getOneKc(String kch);

	List getAll();

}