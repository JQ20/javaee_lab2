package cn.edu.ujn.lab2.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.ujn.lab2.db.HibernateSessionFactory;
import cn.edu.ujn.lab2.model.Dlb;

public class DlbDaoImpl implements IDlbDao{

	public Dlb validate(Dlb dl) {
		Session session = HibernateSessionFactory.getSession();
		Transaction bt = session.beginTransaction();
		Query query = session.createQuery("from Dlb where xh=?1 and kl=?2 ");
		query.setParameter(1, dl.getXh());
		query.setParameter(2, dl.getKl());
		query.setMaxResults(1);
		Dlb dlb = (Dlb) query.uniqueResult();
		
		return dlb;		
	}
}
