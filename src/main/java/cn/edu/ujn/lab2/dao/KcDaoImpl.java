package cn.edu.ujn.lab2.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.ujn.lab2.db.HibernateSessionFactory;
import cn.edu.ujn.lab2.model.Kcb;

public class KcDaoImpl implements KcDao {
	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.KcDao#getOneKc(java.lang.String)
	 */
	@Override
	public Kcb getOneKc(String kch) {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts = session.beginTransaction();
			Query query = session.createQuery("from Kcb where kch=?1");
			query.setParameter(1, kch);
			query.setMaxResults(1);
			Kcb kc=(Kcb) query.uniqueResult();
			ts.commit();
			session.clear();
			return kc;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see cn.edu.ujn.lab2.dao.KcDao#getAll()
	 */
	@Override
	public List getAll() {
		try{
			Session session=HibernateSessionFactory.getSession();
			Transaction ts=session.beginTransaction();
			List list=session.createQuery("from Kcb order by kch").list();
			ts.commit();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
